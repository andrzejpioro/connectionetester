package pl.pioro.conntester.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ConnectionStatusResponse {
    String dstHost, srcHost, exception;
    int dstPort;
    String status;
}
