package pl.pioro.conntester;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConnectionTesterApplication {


	public static void main(String[] args) {
		SpringApplication.run(ConnectionTesterApplication.class, args);
	}
	


}
