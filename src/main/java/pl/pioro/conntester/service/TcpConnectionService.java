package pl.pioro.conntester.service;

import org.springframework.stereotype.Service;
import pl.pioro.conntester.model.ConnectionStatusResponse;

import java.io.IOException;
import java.net.*;

import static java.rmi.server.LogStream.log;

@Service
public class TcpConnectionService {

    /**
     *
     * @param hostName
     * @param port
     * @return boolean - true/false
     */
    public ConnectionStatusResponse checkIfTcpConnectionIsAlive(String hostName, int port) {
        ConnectionStatusResponse response = new ConnectionStatusResponse();
        response.setStatus("FAIL");
        response.setDstHost(hostName);
        response.setDstPort(port);
        try {
            response.setSrcHost(InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        // Creates a socket address from a hostname and a port number
        SocketAddress socketAddress = new InetSocketAddress(hostName, port);
        Socket socket = new Socket();

        // Timeout required - it's in milliseconds
        int timeout = 2000;

        log("hostName: " + hostName + ", port: " + port);
        try {
            socket.connect(socketAddress, timeout);
            socket.close();
            response.setStatus("OK");
        } catch (SocketTimeoutException exception) {
            System.out.println("SocketTimeoutException " + hostName + ":" + port + ". " + exception.getMessage());
            response.setStatus("FAIL");
            response.setException(exception.getMessage());
        } catch (IOException exception) {
            System.out.println(
                    "IOException - Unable to connect to " + hostName + ":" + port + ". " + exception.getMessage());
            response.setStatus("FAIL");
            response.setException(exception.getMessage());
        }
        return response;
    }

}
