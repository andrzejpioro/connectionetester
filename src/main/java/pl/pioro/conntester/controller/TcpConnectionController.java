package pl.pioro.conntester.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.pioro.conntester.model.ConnectionStatusResponse;
import pl.pioro.conntester.service.TcpConnectionService;
@RestController
public class TcpConnectionController {

    @Autowired
    TcpConnectionService tcpConnectionService;



    @GetMapping("/connect")
    public ConnectionStatusResponse checkTcpConnection(@RequestParam String host, @RequestParam int port){
        ConnectionStatusResponse response = tcpConnectionService.checkIfTcpConnectionIsAlive(host,port);
        return tcpConnectionService.checkIfTcpConnectionIsAlive(host,port);
    }

}
